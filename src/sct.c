#include <X11/Xatom.h>
#include <X11/Xlib.h>
#include <X11/Xproto.h>
#include <X11/extensions/Xrandr.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static void usage(char *pname) {
  printf("sct\n"
         "Usage: %s [options] [temperature]\n"
         "\tIf the argument is 0, sct resets the display to the default "
         "temperature (6500K)\n"
         "\tIf no arguments are passed, sct estimates the current display "
         "temperature\n"
         "Options:\n"
         "\t-v, --verbose \t sct will display debugging information\n"
         "\t-d, --delta \t sct will shift temperature by given value\n"
         "\t-h, --help \t sct will display this usage information\n",
         pname);
}

#define TNORM 6500
#define TZERO 700
#define GMULT 65535.0
#define GK0GR -1.47751309139817
#define GK1GR 0.28590164772055
#define GK0BR -4.38321650114872
#define GK1BR 0.6212158769447
#define GK0RB 1.75390204039018
#define GK1RB -0.1150805671482
#define GK0GB 1.49221604915144
#define GK1GB -0.07513509588921

static double dtrim(double x, double a, double b) {
  double buff[3] = {a, x, b};
  return buff[(int)(x > a) + (int)(x > b)];
}

static int getsct(Display *dpy, int screen, int fdebug) {
  Window root = RootWindow(dpy, screen);
  XRRScreenResources *res = XRRGetScreenResourcesCurrent(dpy, root);

  int temp = 0, n, c;
  double t = 0.0;
  double gammar = 0.0, gammag = 0.0, gammab = 0.0, gammam = 1.0, gammad = 0.0;

  n = res->ncrtc;
  for (c = 0; c < n; c++) {
    RRCrtc crtcxid;
    int size;
    XRRCrtcGamma *crtc_gamma;
    crtcxid = res->crtcs[c];
    crtc_gamma = XRRGetCrtcGamma(dpy, crtcxid);
    size = crtc_gamma->size;
    gammar += crtc_gamma->red[size - 1];
    gammag += crtc_gamma->green[size - 1];
    gammab += crtc_gamma->blue[size - 1];

    XRRFreeGamma(crtc_gamma);
  }
  XFree(res);
  gammam = (gammar > gammag) ? gammar : gammag;
  gammam = (gammab > gammam) ? gammab : gammam;
  if (gammam > 0.0) {
    gammar /= gammam;
    gammag /= gammam;
    gammab /= gammam;
    if (fdebug > 0)
      fprintf(stderr, "DEBUG: Gamma: %f, %f, %f\n", gammar, gammag, gammab);
    gammad = gammab - gammar;
    if (gammad < 0.0) {
      if (gammab > 0.0) {
        t = exp((gammag + 1.0 + gammad - (GK0GR + GK0BR)) / (GK1GR + GK1BR)) +
            TZERO;
      } else {
        t = (gammag > 0.0) ? (exp((gammag - GK0GR) / GK1GR) + TZERO) : TZERO;
      }
    } else {
      t = exp((gammag + 1.0 - gammad - (GK0GB + GK0RB)) / (GK1GB + GK1RB)) +
          (TNORM - TZERO);
    }
  }

  temp = (int)(t + 0.5);

  return temp;
}

static void cursct(Display *dpy, int screen, int temp, int fdebug) {
  double t = 0.0, g = 0.0, gammar, gammag, gammab;
  int n, c;
  Window root = RootWindow(dpy, screen);
  XRRScreenResources *res = XRRGetScreenResourcesCurrent(dpy, root);

  t = (double)temp;
  if (temp < TNORM) {
    gammar = 1.0;
    if (temp < TZERO) {
      gammag = 0.0;
      gammab = 0.0;
    } else {
      g = log(t - TZERO);
      gammag = dtrim(GK0GR + GK1GR * g, 0.0, 1.0);
      gammab = dtrim(GK0BR + GK1BR * g, 0.0, 1.0);
    }
  } else {
    g = log(t - (TNORM - TZERO));
    gammar = dtrim(GK0RB + GK1RB * g, 0.0, 1.0);
    gammag = dtrim(GK0GB + GK1GB * g, 0.0, 1.0);
    gammab = 1.0;
  }
  if (fdebug > 0)
    fprintf(stderr, "DEBUG: Gamma: %f, %f, %f\n", gammar, gammag, gammab);

  n = res->ncrtc;
  for (c = 0; c < n; c++) {
    int size, i;
    RRCrtc crtcxid;
    XRRCrtcGamma *crtc_gamma;
    crtcxid = res->crtcs[c];
    size = XRRGetCrtcGammaSize(dpy, crtcxid);

    crtc_gamma = XRRAllocGamma(size);

    for (i = 0; i < size; i++) {
      g = GMULT * (double)i / (double)size;
      crtc_gamma->red[i] = (unsigned short int)(g * gammar + 0.5);
      crtc_gamma->green[i] = (unsigned short int)(g * gammag + 0.5);
      crtc_gamma->blue[i] = (unsigned short int)(g * gammab + 0.5);
    }

    XRRSetCrtcGamma(dpy, crtcxid, crtc_gamma);
    XRRFreeGamma(crtc_gamma);
  }

  XFree(res);
}

int main(int argc, char **argv) {
  int i, screen, screens, temp;
  int fdebug = 0, fdelta = 0, fhelp = 0;
  Display *dpy = XOpenDisplay(NULL);

  if (!dpy) {
    perror("XOpenDisplay(NULL) failed");
    fprintf(stderr, "Make sure DISPLAY is set correctly.\n");
    return EXIT_FAILURE;
  }
  screens = XScreenCount(dpy);

  temp = -1;
  for (i = 1; i < argc; i++) {
    if ((strcmp(argv[i], "-v") == 0) || (strcmp(argv[i], "--verbose") == 0))
      fdebug = 1;
    else if ((strcmp(argv[i], "-d") == 0) || (strcmp(argv[i], "--delta") == 0))
      fdelta = 1;
    else if ((strcmp(argv[i], "-h") == 0) || (strcmp(argv[i], "--help") == 0))
      fhelp = 1;
    else
      temp = atoi(argv[i]);
  }
  if (fhelp > 0) {
    usage(argv[0]);
  } else {
    if ((temp < 0) && (fdelta == 0)) {
      for (screen = 0; screen < screens; screen++) {
        temp = getsct(dpy, screen, fdebug);
        printf("Screen %d: temperature ~ %d\n", screen, temp);
      }
    } else {
      if (fdelta == 0) {
        if (temp == 0) {
          temp = TNORM;
        } else if (temp < TZERO) {
          fprintf(stderr,
                  "WARNING! Temperatures below %d cannot be displayed.\n",
                  TZERO);
          temp = TZERO;
        }
        for (screen = 0; screen < screens; screen++) {
          cursct(dpy, screen, temp, fdebug);
        }
      } else {
        for (screen = 0; screen < screens; screen++) {
          int tempd = temp + getsct(dpy, screen, fdebug);
          if (tempd < TZERO) {
            fprintf(stderr,
                    "WARNING! Temperatures below %d cannot be displayed.\n",
                    TZERO);
            tempd = TZERO;
          }
          cursct(dpy, screen, tempd, fdebug);
        }
      }
    }
  }

  XCloseDisplay(dpy);

  return EXIT_SUCCESS;
}
